package com.soap.client;

import org.itmo.client.BoardGame;
import org.itmo.client.BoardGameService;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

public class Client {
    public static URL url;
    public static Scanner in = new Scanner(System.in);


    public static BoardGameService service;

    public void process(String accessPoint) throws MalformedURLException {
        URL url = new URL(accessPoint);
        service = new BoardGameService(url);
        while (true) {
            printMenu();
            int action = Integer.parseInt(in.nextLine());

            switch (action) {
                case 1:
                    getAllGames();
                    break;
                case 2:
                    getGame();
                    break;
                case 3:
                    deleteGame();
                    break;
                case 4:
                    updateGame();
                    break;
                case 5:
                    createGame();
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Действия не существует!");
                    break;
            }

        }

    }

    private static void getAllGames() {
        List<BoardGame> res = service.getBoardGameWebServicePort().getAllBoardGames();
        printBoardGames(res);
    }

    private static void getGame() {
        System.out.println("Введите  id записи");
        Long id = Long.parseLong(in.nextLine());
        BoardGame res = service.getBoardGameWebServicePort().getBoardGame(id);
        printBoardGame(res);
    }

    private static void deleteGame() {
        System.out.println("Введите  id записи");
        Long id = Long.parseLong(in.nextLine());
        boolean res = service.getBoardGameWebServicePort().deleteBoardGames(id);
        System.out.println(res ? "Успех" : "Ошибка");
    }

    private static void updateGame() {
        System.out.println("Введите  id записи");
        Long id = Long.parseLong(in.nextLine());
        System.out.println("Введите  имя");
        String name = in.nextLine();
        System.out.println("Введите  жанр");
        String genre = in.nextLine();
        System.out.println("Введите  стоимость");
        Integer price = Integer.parseInt(in.nextLine());
        System.out.println("Введите  минимальное количество игроков");
        Integer minPlayers = Integer.parseInt(in.nextLine());
        System.out.println("Введите  максимальное количество игроков");
        Integer maxPlayers = Integer.parseInt(in.nextLine());
        boolean res = service.getBoardGameWebServicePort().updateBoardGames(id, name, genre, price, minPlayers, maxPlayers);
        System.out.println(res ? "Успех" : "Ошибка");
    }

    private static void createGame() {
        System.out.println("Введите  имя");
        String name = in.nextLine();
        System.out.println("Введите жанр");
        String genre = in.nextLine();
        System.out.println("Введите  стоимость");
        Integer price = Integer.parseInt(in.nextLine());
        System.out.println("Введите  минимальное количество игроков");
        Integer minPlayers = Integer.parseInt(in.nextLine());
        System.out.println("Введите  максимальное количество игроков");
        Integer maxPlayers = Integer.parseInt(in.nextLine());
        Long res = service.getBoardGameWebServicePort().createBoardGames(name, genre, price, minPlayers, maxPlayers);
        System.out.printf("Id новой записи - %s%n", res);
    }


    private static void printMenu() {
        System.out.println(
                "Выбирите действие:\n" +
                        "1. Вывести все игры\n" +
                        "2. Вывести игру по id\n" +
                        "3. Удалить игру\n" +
                        "4. Изменить игру\n" +
                        "5. Создать игру\n" +
                        "0. Выход\n"
        );
    }

    private static void printBoardGames(List<BoardGame> result) {
        System.out.printf("Total rows %d\n", result.size());
        result.forEach(Client::printBoardGame);

    }

    private static void printBoardGame(BoardGame boardGame) {
        if (boardGame == null) {
            System.out.println("null");
            return;
        }
        System.out.printf("Name: '%s'\t", boardGame.getName());
        System.out.printf("Genre: '%s'\t", boardGame.getGenre());
        System.out.printf("Price: '%d'\t", boardGame.getPrice());
        System.out.printf("Min players: '%d'\t", boardGame.getMinPlayers());
        System.out.printf("Max players: '%d'\n", boardGame.getMaxPlayers());
    }
}