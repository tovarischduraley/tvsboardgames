
package org.itmo.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.itmo.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateBoardGames_QNAME = new QName("http://itmo.org/", "createBoardGames");
    private final static QName _CreateBoardGamesResponse_QNAME = new QName("http://itmo.org/", "createBoardGamesResponse");
    private final static QName _DeleteBoardGames_QNAME = new QName("http://itmo.org/", "deleteBoardGames");
    private final static QName _DeleteBoardGamesResponse_QNAME = new QName("http://itmo.org/", "deleteBoardGamesResponse");
    private final static QName _GetAllBoardGames_QNAME = new QName("http://itmo.org/", "getAllBoardGames");
    private final static QName _GetAllBoardGamesResponse_QNAME = new QName("http://itmo.org/", "getAllBoardGamesResponse");
    private final static QName _GetBoardGame_QNAME = new QName("http://itmo.org/", "getBoardGame");
    private final static QName _GetBoardGameResponse_QNAME = new QName("http://itmo.org/", "getBoardGameResponse");
    private final static QName _SearchBoardGames_QNAME = new QName("http://itmo.org/", "searchBoardGames");
    private final static QName _SearchBoardGamesResponse_QNAME = new QName("http://itmo.org/", "searchBoardGamesResponse");
    private final static QName _UpdateBoardGames_QNAME = new QName("http://itmo.org/", "updateBoardGames");
    private final static QName _UpdateBoardGamesResponse_QNAME = new QName("http://itmo.org/", "updateBoardGamesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.itmo.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateBoardGames }
     * 
     */
    public CreateBoardGames createCreateBoardGames() {
        return new CreateBoardGames();
    }

    /**
     * Create an instance of {@link CreateBoardGamesResponse }
     * 
     */
    public CreateBoardGamesResponse createCreateBoardGamesResponse() {
        return new CreateBoardGamesResponse();
    }

    /**
     * Create an instance of {@link DeleteBoardGames }
     * 
     */
    public DeleteBoardGames createDeleteBoardGames() {
        return new DeleteBoardGames();
    }

    /**
     * Create an instance of {@link DeleteBoardGamesResponse }
     * 
     */
    public DeleteBoardGamesResponse createDeleteBoardGamesResponse() {
        return new DeleteBoardGamesResponse();
    }

    /**
     * Create an instance of {@link GetAllBoardGames }
     * 
     */
    public GetAllBoardGames createGetAllBoardGames() {
        return new GetAllBoardGames();
    }

    /**
     * Create an instance of {@link GetAllBoardGamesResponse }
     * 
     */
    public GetAllBoardGamesResponse createGetAllBoardGamesResponse() {
        return new GetAllBoardGamesResponse();
    }

    /**
     * Create an instance of {@link GetBoardGame }
     * 
     */
    public GetBoardGame createGetBoardGame() {
        return new GetBoardGame();
    }

    /**
     * Create an instance of {@link GetBoardGameResponse }
     * 
     */
    public GetBoardGameResponse createGetBoardGameResponse() {
        return new GetBoardGameResponse();
    }

    /**
     * Create an instance of {@link SearchBoardGames }
     * 
     */
    public SearchBoardGames createSearchBoardGames() {
        return new SearchBoardGames();
    }

    /**
     * Create an instance of {@link SearchBoardGamesResponse }
     * 
     */
    public SearchBoardGamesResponse createSearchBoardGamesResponse() {
        return new SearchBoardGamesResponse();
    }

    /**
     * Create an instance of {@link UpdateBoardGames }
     * 
     */
    public UpdateBoardGames createUpdateBoardGames() {
        return new UpdateBoardGames();
    }

    /**
     * Create an instance of {@link UpdateBoardGamesResponse }
     * 
     */
    public UpdateBoardGamesResponse createUpdateBoardGamesResponse() {
        return new UpdateBoardGamesResponse();
    }

    /**
     * Create an instance of {@link BoardGame }
     * 
     */
    public BoardGame createBoardGame() {
        return new BoardGame();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateBoardGames }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateBoardGames }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "createBoardGames")
    public JAXBElement<CreateBoardGames> createCreateBoardGames(CreateBoardGames value) {
        return new JAXBElement<CreateBoardGames>(_CreateBoardGames_QNAME, CreateBoardGames.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateBoardGamesResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateBoardGamesResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "createBoardGamesResponse")
    public JAXBElement<CreateBoardGamesResponse> createCreateBoardGamesResponse(CreateBoardGamesResponse value) {
        return new JAXBElement<CreateBoardGamesResponse>(_CreateBoardGamesResponse_QNAME, CreateBoardGamesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteBoardGames }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteBoardGames }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "deleteBoardGames")
    public JAXBElement<DeleteBoardGames> createDeleteBoardGames(DeleteBoardGames value) {
        return new JAXBElement<DeleteBoardGames>(_DeleteBoardGames_QNAME, DeleteBoardGames.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteBoardGamesResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteBoardGamesResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "deleteBoardGamesResponse")
    public JAXBElement<DeleteBoardGamesResponse> createDeleteBoardGamesResponse(DeleteBoardGamesResponse value) {
        return new JAXBElement<DeleteBoardGamesResponse>(_DeleteBoardGamesResponse_QNAME, DeleteBoardGamesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllBoardGames }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetAllBoardGames }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "getAllBoardGames")
    public JAXBElement<GetAllBoardGames> createGetAllBoardGames(GetAllBoardGames value) {
        return new JAXBElement<GetAllBoardGames>(_GetAllBoardGames_QNAME, GetAllBoardGames.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllBoardGamesResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetAllBoardGamesResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "getAllBoardGamesResponse")
    public JAXBElement<GetAllBoardGamesResponse> createGetAllBoardGamesResponse(GetAllBoardGamesResponse value) {
        return new JAXBElement<GetAllBoardGamesResponse>(_GetAllBoardGamesResponse_QNAME, GetAllBoardGamesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBoardGame }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetBoardGame }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "getBoardGame")
    public JAXBElement<GetBoardGame> createGetBoardGame(GetBoardGame value) {
        return new JAXBElement<GetBoardGame>(_GetBoardGame_QNAME, GetBoardGame.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBoardGameResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetBoardGameResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "getBoardGameResponse")
    public JAXBElement<GetBoardGameResponse> createGetBoardGameResponse(GetBoardGameResponse value) {
        return new JAXBElement<GetBoardGameResponse>(_GetBoardGameResponse_QNAME, GetBoardGameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchBoardGames }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchBoardGames }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "searchBoardGames")
    public JAXBElement<SearchBoardGames> createSearchBoardGames(SearchBoardGames value) {
        return new JAXBElement<SearchBoardGames>(_SearchBoardGames_QNAME, SearchBoardGames.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchBoardGamesResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SearchBoardGamesResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "searchBoardGamesResponse")
    public JAXBElement<SearchBoardGamesResponse> createSearchBoardGamesResponse(SearchBoardGamesResponse value) {
        return new JAXBElement<SearchBoardGamesResponse>(_SearchBoardGamesResponse_QNAME, SearchBoardGamesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateBoardGames }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateBoardGames }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "updateBoardGames")
    public JAXBElement<UpdateBoardGames> createUpdateBoardGames(UpdateBoardGames value) {
        return new JAXBElement<UpdateBoardGames>(_UpdateBoardGames_QNAME, UpdateBoardGames.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateBoardGamesResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateBoardGamesResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://itmo.org/", name = "updateBoardGamesResponse")
    public JAXBElement<UpdateBoardGamesResponse> createUpdateBoardGamesResponse(UpdateBoardGamesResponse value) {
        return new JAXBElement<UpdateBoardGamesResponse>(_UpdateBoardGamesResponse_QNAME, UpdateBoardGamesResponse.class, null, value);
    }

}
