package org.itmo.web;

public class ValidationException extends RuntimeException {
    private final BoarGameServiceFault fault;

    public ValidationException(String message, BoarGameServiceFault fault) {
        super(message);
        this.fault = fault;
    }

    public ValidationException(String message) {
        super(message);
        this.fault = BoarGameServiceFault.defaultInstance(message);
    }

    public ValidationException(String message, BoarGameServiceFault fault, Throwable cause) {
        super(message, cause);
        this.fault = fault;
    }

    public BoarGameServiceFault getFaultInfo() {
        return fault;
    }
}
