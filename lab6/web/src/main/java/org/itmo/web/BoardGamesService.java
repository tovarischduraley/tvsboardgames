package org.itmo.web;


import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/games")
public class BoardGamesService {

    private final BoardGameDAO boardGameDAO;

    public BoardGamesService(BoardGameDAO boardGameDAO) {
        this.boardGameDAO = boardGameDAO;
    }

    @GetMapping("/search")
    public List<BoardGame> searchBoardGame(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String genre,
            @RequestParam(required = false) Integer price,
            @RequestParam(required = false) Integer players) {
        return boardGameDAO.searchBoardGames(name, genre, price, players);
    }

    @GetMapping("/{gameId}")
    public BoardGame searchBoardGame(@PathVariable() Long gameId) {
        validateId(gameId);
        return boardGameDAO.getBoardGame(gameId);
    }

    @GetMapping
    public List<BoardGame> getAllBoardGames() {
        return boardGameDAO.getAllBoardGames();
    }

    @PostMapping
    public Long createBoardGame(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String genre,
            @RequestParam(required = false) Integer price,
            @RequestParam(required = false) Integer minPlayers,
            @RequestParam(required = false) Integer maxPlayers) {
        validateGenre(genre);
        validateName(name);
        validatePrice(price);
        validatePlayersAmount(minPlayers, maxPlayers);
        return boardGameDAO.createBoardGame(name, genre, price, minPlayers, maxPlayers);
    }

    @PutMapping("/{gameId}")
    public boolean updateGame(
            @PathVariable Long gameId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String genre,
            @RequestParam(required = false) Integer price,
            @RequestParam(required = false) Integer minPlayers,
            @RequestParam(required = false) Integer maxPlayers) {
        validateId(gameId);
        validateGenre(genre);
        validateName(name);
        validatePrice(price);
        validatePlayersAmount(minPlayers, maxPlayers);
        return boardGameDAO.updateBoardGames(name, genre, price, minPlayers, maxPlayers, gameId);
    }

    @DeleteMapping("/{gameId}")
    public boolean deleteBoardGame(@PathVariable Long gameId) {
        validateId(gameId);
        return boardGameDAO.deleteBoardGame(gameId);
    }

    private void validateName(String name) throws ValidationException {
        if (name == null || name.isBlank()) {
            throw new ValidationException("Некорректное значение поля name");
        }
    }

    private void validateGenre(String genre) throws ValidationException {
        if (genre == null || genre.isBlank()) {
            throw new ValidationException("Некорректное значение поля name");
        }
    }

    private void validateId(Long id) throws ValidationException {
        if (id == null) {
            throw new ValidationException("ID не может быть пустым");
        }
    }

    private void validatePlayersAmount(Integer minPlayers, Integer maxPlayers) throws ValidationException {
        if (minPlayers == null || maxPlayers == null) {
            return;
        }
        if (minPlayers > maxPlayers) {
            throw new ValidationException("Максимальное количество игроков не может быть больше минимального");
        }
    }

    private void validatePrice(Integer price) throws ValidationException {
        if (price == null) {
            return;
        }
        if (price < 0) {
            throw new ValidationException("Цена не может быть отрицательной");
        }
    }

}
