package org.itmo.web;

public class ResourceNotFoundException extends RuntimeException {
    private final BoarGameServiceFault fault;

    public ResourceNotFoundException(String message, BoarGameServiceFault fault) {
        super(message);
        this.fault = fault;
    }

    public ResourceNotFoundException(String message) {
        super(message);
        this.fault = BoarGameServiceFault.defaultInstance(message);
    }

    public ResourceNotFoundException(String message, BoarGameServiceFault fault, Throwable cause) {
        super(message, cause);
        this.fault = fault;
    }

    public BoarGameServiceFault getFaultInfo() {
        return fault;
    }
}
