package org.itmo;

public class BoardGame {
    private String name;
    private String genre;
    private Integer maxPlayers;
    private Integer minPlayers;
    private Integer price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public Integer getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(Integer minPlayers) {
        this.minPlayers = minPlayers;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public BoardGame(String name, String genre, Integer price, Integer maxPlayers, Integer minPlayers) {
        this.name = name;
        this.genre = genre;
        this.price = price;
        this.maxPlayers = maxPlayers;
        this.minPlayers = minPlayers;
    }

    public BoardGame() {
    }

    @Override
    public String toString() {
        return "BoardGame{" +
                "name='" + name + '\'' +
                ", genre='" + genre + '\'' +
                ", maxPlayers=" + maxPlayers +
                ", minPlayers=" + minPlayers +
                ", price=" + price +
                '}';
    }
}
