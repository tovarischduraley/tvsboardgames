package org.itmo;

public class BoarGameServiceFault {

    private String message;

    public BoarGameServiceFault(String message) {
        this.message = message;
    }

    public BoarGameServiceFault() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static BoarGameServiceFault defaultInstance(String message) {
        return new BoarGameServiceFault(message);
    }
}
