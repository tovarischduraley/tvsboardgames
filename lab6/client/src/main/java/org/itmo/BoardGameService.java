package org.itmo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import javax.ws.rs.core.MediaType;
import java.util.List;

public class BoardGameService {
    private Client client = Client.create();
    private ObjectMapper objectMapper = new ObjectMapper();

    List<BoardGame> getBoardGames(String name, String genre, Integer price, Integer players) {
        String URL = "http://localhost:8080/games/search";
        WebResource webResource = client.resource(URL);
        webResource.addFilter(new HTTPBasicAuthFilter("user", "user"));
        var quueryParam = new MultivaluedMapImpl();
        if (name != null) {
            quueryParam.add("name", name);
        }
        if (genre != null) {
            quueryParam.add("genre", genre);
        }
        if (price != null) {
            quueryParam.add("price", price.toString());
        }
        if (players != null) {
            quueryParam.add("players", players.toString());
        }
        if (!quueryParam.isEmpty()) {
            webResource = webResource.queryParams(quueryParam);
        }

        ClientResponse response =
                webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        return mapResponseToBoardGameList(response);
    }

    public Boolean updateBoardGames(Long id, String name, String genre, Integer price, Integer minPlayers, Integer maxPlayers) {
        String URL = "http://localhost:8080/games/" + id.toString();
        WebResource webResource = client.resource(URL);
        webResource.addFilter(new HTTPBasicAuthFilter("user", "user"));
        var quueryParam = new MultivaluedMapImpl();
        if (name != null) {
            quueryParam.add("name", name);
        }
        if (genre != null) {
            quueryParam.add("genre", genre);
        }
        if (price != null) {
            quueryParam.add("price", price.toString());
        }
        if (minPlayers != null) {
            quueryParam.add("minPlayers", minPlayers.toString());
        }
        if (maxPlayers != null) {
            quueryParam.add("maxPlayers", maxPlayers.toString());
        }
        if (!quueryParam.isEmpty()) {
            webResource = webResource.queryParams(quueryParam);
        }

        ClientResponse response =
                webResource.accept(MediaType.APPLICATION_JSON).put(ClientResponse.class);
        return mapResponseToBoolean(response);
    }

    public Long createBoardGames(String name, String genre, Integer price, Integer minPlayers, Integer maxPlayers) {
        String URL = "http://localhost:8080/games";
        WebResource webResource = client.resource(URL);
        webResource.addFilter(new HTTPBasicAuthFilter("user", "user"));
        var quueryParam = new MultivaluedMapImpl();
        if (name != null) {
            quueryParam.add("name", name);
        }
        if (genre != null) {
            quueryParam.add("genre", genre);
        }
        if (price != null) {
            quueryParam.add("price", price.toString());
        }
        if (minPlayers != null) {
            quueryParam.add("minPlayers", minPlayers.toString());
        }
        if (maxPlayers != null) {
            quueryParam.add("maxPlayers", maxPlayers.toString());
        }
        if (!quueryParam.isEmpty()) {
            webResource = webResource.queryParams(quueryParam);
        }

        ClientResponse response =
                webResource.accept(MediaType.APPLICATION_JSON).post(ClientResponse.class);
        return mapResponseToLong(response);
    }

    public List<BoardGame> getAllBoardGames() {
        String URL = "http://localhost:8080/games";
        WebResource webResource = client.resource(URL);
        webResource.addFilter(new HTTPBasicAuthFilter("user", "user"));
        var response = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        return mapResponseToBoardGameList(response);
    }

    public BoardGame getBoardGame(Long id) {
        String URL = "http://localhost:8080/games/" + id.toString();
        WebResource webResource = client.resource(URL);
        webResource.addFilter(new HTTPBasicAuthFilter("user", "user"));
        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        return mapResponseToBoardGame(response);
    }

    public Boolean deleteBoardGames(Long id) {
        String URL = "http://localhost:8080/games/" + id.toString();
        WebResource webResource = client.resource(URL);
        webResource.addFilter(new HTTPBasicAuthFilter("user", "user"));
        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).delete(ClientResponse.class);
        return mapResponseToBoolean(response);
    }

    private BoardGame mapResponseToBoardGame(ClientResponse response) {
        if (response.getStatus() !=
                ClientResponse.Status.OK.getStatusCode()) {
            throw new IllegalStateException(getErrorMessage(response));
        }
        var type = new TypeReference<BoardGame>() {
        };
        try {
            return objectMapper.readValue(response.getEntity(String.class), type);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private List<BoardGame> mapResponseToBoardGameList(ClientResponse response) {
        if (response.getStatus() !=
                ClientResponse.Status.OK.getStatusCode()) {
            throw new IllegalStateException(getErrorMessage(response));
        }
        var type = new TypeReference<List<BoardGame>>() {
        };
        try {
            return objectMapper.readValue(response.getEntity(String.class), type);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private Long mapResponseToLong(ClientResponse response) {
        if (response.getStatus() !=
                ClientResponse.Status.OK.getStatusCode()) {
            throw new IllegalStateException(getErrorMessage(response));
        }
        try {
            return objectMapper.readValue(response.getEntity(String.class), Long.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private Boolean mapResponseToBoolean(ClientResponse response) {
        if (response.getStatus() !=
                ClientResponse.Status.OK.getStatusCode()) {
            throw new IllegalStateException(getErrorMessage(response));
        }
        try {
            return objectMapper.readValue(response.getEntity(String.class), Boolean.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private String getErrorMessage(ClientResponse response) {
        try {
            return objectMapper.readValue(response.getEntity(String.class), BoarGameServiceFault.class).getMessage();
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }
}