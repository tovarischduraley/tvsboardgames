package org.itmo.web;


import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/games")
public class BoardGamesService {

    private final BoardGameDAO boardGameDAO;

    public BoardGamesService(BoardGameDAO boardGameDAO) {
        this.boardGameDAO = boardGameDAO;
    }

    @GetMapping("/search")
    public List<BoardGame> searchBoardGame(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String genre,
            @RequestParam(required = false) Integer price,
            @RequestParam(required = false) Integer players) {
        return boardGameDAO.searchBoardGames(name, genre, price, players);
    }

    @GetMapping("/{gameId}")
    public BoardGame searchBoardGame(@PathVariable Long gameId) {
        return boardGameDAO.getBoardGame(gameId);
    }

    @GetMapping
    public List<BoardGame> getAllBoardGames() {
        return boardGameDAO.getAllBoardGames();
    }

    @PostMapping
    public Long searchBoardGame(@RequestParam(required = false) String name,
                                @RequestParam(required = false) String genre,
                                @RequestParam(required = false) Integer price,
                                @RequestParam(required = false) Integer minPlayers,
                                @RequestParam(required = false) Integer maxPlayers) {
        return boardGameDAO.createBoardGame(name, genre, price, minPlayers, maxPlayers);
    }

    @PutMapping("/{gameId}")
    public boolean updateGame(
            @PathVariable() Long gameId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String genre,
            @RequestParam(required = false) Integer price,
            @RequestParam(required = false) Integer minPlayers,
            @RequestParam(required = false) Integer maxPlayers) {
        return boardGameDAO.updateBoardGames(name, genre, price, minPlayers, maxPlayers, gameId);
    }

    @DeleteMapping("/{gameId}")
    public boolean deleteBoardGame(@PathVariable() Long gameId) {
        return boardGameDAO.deleteBoardGame(gameId);
    }

}
