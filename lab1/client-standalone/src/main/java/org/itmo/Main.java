package org.itmo;

import org.itmo.client.BoardGame;
import org.itmo.client.BoardGameService;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class Main {
    public static void main(String[] args) throws MalformedURLException {
        var url = new URL("http://localhost:8081/BoardGameService?wsdl");
        var service = new BoardGameService(url);

        // Select by name game.name = input
        System.out.println("Select by name game.name = Inish");
        var res1 = service.getBoardGameWebServicePort().getBoardGames("Inish", null, null, null);
        printResult(res1);

        // Select by Price game.price <= input
        System.out.println("Select by Price game.price <= 2500");
        var res2 = service.getBoardGameWebServicePort().getBoardGames(null, null, 2500, null);
        printResult(res2);

        // Select by players game.minPlayer <= input && game.maxPlayers >= input
        System.out.println("Select by players game.minPlayer <= 8 && game.maxPlayers >= 8");
        var res3 = service.getBoardGameWebServicePort().getBoardGames(null, null, null, 8);
        printResult(res3);

        // Select by genre, price and players
        System.out.println("Select by genre, price and players");
        System.out.println("----game.genre = 'Party Game' && game.price <= 2500 && game.minPlayer <= 8 && game.maxPlayers >= 8");
        var res4 = service.getBoardGameWebServicePort().getBoardGames(null, "Party Game", 1000, 5);
        printResult(res4);

    }

    public static void printResult(List<BoardGame> result) {
        System.out.printf("Total rows %d\n", result.size());
        for (int i = 0; i < result.size(); i++) {
            System.out.printf("%d) ", i + 1);
            System.out.printf("Name: '%s'\t", result.get(i).getName());
            System.out.printf("Genre: '%s'\t", result.get(i).getGenre());
            System.out.printf("Price: '%d'\t", result.get(i).getPrice());
            System.out.printf("Min players: '%d'\t", result.get(i).getMinPlayers());
            System.out.printf("Max players: '%d'\t", result.get(i).getMaxPlayers());
            System.out.printf("Max players: '%d'\n", result.get(i).getMaxPlayers());
        }
        System.out.println();
    }
}