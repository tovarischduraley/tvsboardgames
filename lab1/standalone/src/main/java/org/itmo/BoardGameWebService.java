package org.itmo;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(serviceName = "BoardGameService")
public class BoardGameWebService {
    @WebMethod(operationName = "getBoardGames")
    public List<BoardGame> getBoardGames(
            String name,
            String genre,
            Integer price,
            Integer players
    ) {
        BoardGameDAO dao = new BoardGameDAO();
        return dao.getBoardGames(name, genre, price, players);
    }
}