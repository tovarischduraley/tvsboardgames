package org.itmo;
import javax.xml.ws.Endpoint;


public class Main {
    public static void main(String[] args) {
        String url = "http://0.0.0.0:8081/BoardGameService";
        Endpoint.publish(url, new BoardGameWebService());
    }
}