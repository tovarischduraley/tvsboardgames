package org.itmo.webapp;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebService(serviceName = "BoardGameService")
public class BoardGameWebService {

    private DataSource dataSource;

    public BoardGameWebService() {
        try {
            InitialContext initialContext = new InitialContext();
            dataSource = (DataSource) initialContext.lookup("java:/comp/env/jdbc/board-games-db");
        } catch (Exception e) {
            System.err.println("Init error " + e);
        }
    }

    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            Logger.getLogger(BoardGameDAO.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @WebMethod(operationName = "getBoardGames")
    public List<BoardGame> getBoardGames(
            String name,
            String genre,
            Integer price,
            Integer players
    ) {
        Connection connection = getConnection();
        BoardGameDAO dao = new BoardGameDAO(connection);
        System.out.println(connection);
        return dao.getBoardGames(name, genre, price, players);
    }

    private Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            Logger.getLogger(BoardGameWebService.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }
}
