package org.itmo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import javax.ws.rs.core.MediaType;
import java.util.List;

public class BoardGameService {
    private String URL = "http://localhost:8081/rest/games";
    private Client client = Client.create();
    private ObjectMapper objectMapper = new ObjectMapper();

    List<BoardGame> getBoardGames(String name, String genre, Integer price, Integer players) {
        WebResource webResource = client.resource(URL);
        var quueryParam = new MultivaluedMapImpl();
        if (name != null) {
            quueryParam.add("name", name);
        }
        if (genre != null) {
            quueryParam.add("genre", genre);
        }
        if (price != null) {
            quueryParam.add("price", price.toString());
        }
        if (players != null) {
            quueryParam.add("players", players.toString());
        }
        if (!quueryParam.isEmpty()) {
            webResource = webResource.queryParams(quueryParam);
        }

        ClientResponse response =
                webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() !=
                ClientResponse.Status.OK.getStatusCode()) {
            throw new IllegalStateException("Request failed");
        }
        var type = new TypeReference<List<BoardGame>>() {
        };
        try {
            return objectMapper.readValue(response.getEntity(String.class), type);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}