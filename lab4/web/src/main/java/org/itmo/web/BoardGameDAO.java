package org.itmo.web;

import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class BoardGameDAO {
    private final DataSource dataSource;

    public BoardGameDAO(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<BoardGame> getBoardGames(
            String name,
            String genre,
            Integer price,
            Integer players
    ) {
        List<BoardGame> games = new ArrayList<>();
        String query = buildQuery(name, genre, price, players);
        System.out.println(query);
        try (PreparedStatement ps = dataSource.getConnection().prepareStatement(query)) {
            int i = 1;
            if (name != null && !name.isEmpty()) {
                ps.setString(i++, name);
            }
            if (genre != null && !genre.isEmpty()) {
                ps.setString(i++, genre);
            }
            if (price != null && price > 0) {
                ps.setInt(i++, price);
            }
            if (players != null && players > 0) {
                ps.setInt(i++, players);
                ps.setInt(i, players);
            }

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String Name = rs.getString("name");
                String Genre = rs.getString("genre");
                int Price = rs.getInt("price");
                int maxPlayers = rs.getInt("max_players");
                int minPlayers = rs.getInt("min_players");
                BoardGame game = new BoardGame(Name, Genre, Price, maxPlayers, minPlayers);
                games.add(game);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BoardGameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return games;
    }

    private String buildQuery(
            String name,
            String genre,
            Integer price,
            Integer players
    ) {
        StringBuilder query = new StringBuilder("select * from boardgames where 1=1");
        if (name != null && !name.isEmpty()) {
            query.append(" and name = ?");
        }
        if (genre != null && !genre.isEmpty()) {
            query.append(" and genre = ?");
        }
        if (price != null && price > 0) {
            query.append(" and price <= ?");
        }
        if (players != null && players > 0) {
            query.append(" and (max_players >= ? or max_players is null)");
            query.append(" and min_players <= ?");
        }
        return query.toString();
    }
}
