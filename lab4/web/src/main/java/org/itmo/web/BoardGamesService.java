package org.itmo.web;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class BoardGamesService {

    private final BoardGameDAO boardGameDAO;

    public BoardGamesService(BoardGameDAO boardGameDAO) {
        this.boardGameDAO = boardGameDAO;
    }

    @GetMapping("/games")
    public List<BoardGame> searchBoardGame(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String genre,
            @RequestParam(required = false) Integer price,
            @RequestParam(required = false) Integer players) {
        return boardGameDAO.getBoardGames(name, genre, price, players);
    }
}
