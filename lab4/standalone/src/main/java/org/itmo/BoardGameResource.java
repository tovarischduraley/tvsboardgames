package org.itmo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/games")
public class BoardGameResource {

    private ObjectMapper objectMapper = new ObjectMapper();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getBoardGames(
            @QueryParam("name") String name,
            @QueryParam("genre") String genre,
            @QueryParam("price") Integer price,
            @QueryParam("players") Integer players
    ) throws JsonProcessingException {
        BoardGameDAO dao = new BoardGameDAO();
        return objectMapper.writeValueAsString(dao.getBoardGames(name, genre, price, players));
    }


}