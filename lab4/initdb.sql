CREATE TABLE boardgames (
   id bigserial not null primary key,
   name varchar,
   genre varchar,
   max_players integer,
   min_players integer,
   price integer
);


INSERT INTO boardgames(name, genre, price, min_players, max_players) values ('Root', 'Area Control', 4000, 2, 4);
INSERT INTO boardgames(name, genre, price, min_players, max_players) values ('Inish', 'Area Control', 5500, 2, 4);
INSERT INTO boardgames(name, genre, price, min_players, max_players) values ('Eldritch Horror', 'RPG', 4990, 1, 8);
INSERT INTO boardgames(name, genre, price, min_players, max_players) values ('Cosmic encounter', 'RPG', 5990, 3, 5);
INSERT INTO boardgames(name, genre, price, min_players) values ('Alias', 'Party Game', 1500, 4);
INSERT INTO boardgames(name, genre, price, min_players, max_players) values ('Merchants Cove', 'EURO', 5490, 1, 4);
INSERT INTO boardgames(name, genre, price, min_players, max_players) values ('Svintus 2.0', 'Party Game', 1290, 2, 10);
INSERT INTO boardgames(name, genre, price, min_players, max_players) values ('Explosive Cats', 'Party Game', 990, 2, 5);
INSERT INTO boardgames(name, genre, price, min_players, max_players) values ('Not Alone', 'Bluffing', 1990, 2, 7);
INSERT INTO boardgames(name, genre, price, min_players, max_players) values ('Citadels', 'City Building', 2990, 2, 8);
INSERT INTO boardgames(name, genre, price, min_players, max_players) values ('Star Realms', 'Deck Building', 990, 2, 2);
