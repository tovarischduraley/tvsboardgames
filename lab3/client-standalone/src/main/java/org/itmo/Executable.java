package org.itmo;

import org.itmo.client.ResourceNotFoundException;
import org.itmo.client.ValidationException;

@FunctionalInterface
public interface Executable<T> {

    T execute() throws ValidationException, ResourceNotFoundException;

}
