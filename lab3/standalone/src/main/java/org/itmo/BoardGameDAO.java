package org.itmo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BoardGameDAO {

    public List<BoardGame> searchBoardGames(
            String name,
            String genre,
            Integer price,
            Integer players
    ) {
        List<BoardGame> games = new ArrayList<>();
        try (Connection connection = DBConnection.getConnection()) {
            String query = buildGetQuery(name, genre, price, players);
            System.out.println(query);
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                int i = 1;
                if (name != null && !name.isEmpty()) {
                    ps.setString(i++, name);
                }
                if (genre != null && !genre.isEmpty()) {
                    ps.setString(i++, genre);
                }
                if (price != null && price > 0) {
                    ps.setInt(i++, price);
                }
                if (players != null && players > 0) {
                    ps.setInt(i++, players);
                    ps.setInt(i, players);
                }

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    var game = mapResultSetToBoardGame(rs);
                    games.add(game);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(BoardGameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return games;
    }

    public Long createBoardGame(String name, String genre, Integer price, Integer minPlayers, Integer maxPlayers) {
        var createQuery = "insert into boardgames (name, genre, price, min_players, max_players) values (?, ?, ?, ?, ?)";
        try (Connection connection = DBConnection.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(createQuery, Statement.RETURN_GENERATED_KEYS)) {
                int i = 1;
                ps.setString(i++, name);
                ps.setString(i++, genre);
                ps.setInt(i++, price);
                ps.setInt(i++, minPlayers);
                ps.setInt(i, maxPlayers);
                ps.executeUpdate();
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getLong("id");
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(BoardGameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public boolean deleteBoardGame(long id) throws ResourceNotFoundException {
        var deleteQuery = "delete from boardgames bg where bg.id = ?";
        try (Connection connection = DBConnection.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
                ps.setLong(1, id);
                return checkQueryResult(ps.executeUpdate(), id);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BoardGameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public BoardGame getBoardGame(long id) {
        var getQuery = "select * from boardgames bg where bg.id = ?";
        try (Connection connection = DBConnection.getConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(getQuery)) {
                ps.setLong(1, id);
                var rs = ps.executeQuery();
                rs.next();
                return mapResultSetToBoardGame(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BoardGameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    public List<BoardGame> getAllBoardGames() {
        var getAllQuery = "select * from boardgames";
        var games = new ArrayList<BoardGame>();
        try (Connection connection = DBConnection.getConnection()) {
            try (var statement = connection.createStatement()) {
                ResultSet rs = statement.executeQuery(getAllQuery);
                while (rs.next()) {
                    var game = mapResultSetToBoardGame(rs);
                    games.add(game);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(BoardGameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return games;
    }

    public boolean updateBoardGames(
            String name,
            String genre,
            Integer price,
            Integer minPlayers,
            Integer maxPlayers,
            long id
    ) throws ResourceNotFoundException {
        var updateQuery = """
                update boardgames bg set name = ?, genre = ?, min_players = ?,
                 max_players = ?, price = ? where bg.id = ?
                 """;
        try (Connection connection = DBConnection.getConnection()) {
            try (var ps = connection.prepareStatement(updateQuery)) {
                var i = 1;
                ps.setString(i++, name);
                ps.setString(i++, genre);
                ps.setInt(i++, minPlayers);
                ps.setInt(i++, maxPlayers);
                ps.setInt(i++, price);
                ps.setLong(i, id);
                return checkQueryResult(ps.executeUpdate(), id);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BoardGameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private BoardGame mapResultSetToBoardGame(ResultSet rs) throws SQLException {
        String Name = rs.getString("name");
        String Genre = rs.getString("genre");
        int Price = rs.getInt("price");
        int maxPlayers = rs.getInt("max_players");
        int minPlayers = rs.getInt("min_players");
        return new BoardGame(Name, Genre, Price, maxPlayers, minPlayers);
    }


    private String buildGetQuery(
            String name,
            String genre,
            Integer price,
            Integer players
    ) {
        StringBuilder query = new StringBuilder("select * from boardgames where 1=1");
        if (name != null && !name.isEmpty()) {
            query.append(" and name = ?");
        }
        if (genre != null && !genre.isEmpty()) {
            query.append(" and genre = ?");
        }
        if (price != null && price > 0) {
            query.append(" and price <= ?");
        }
        if (players != null && players > 0) {
            query.append(" and (max_players >= ? or max_players is null)");
            query.append(" and min_players <= ?");
        }
        return query.toString();
    }

    private boolean checkQueryResult(int res, Number id) throws ResourceNotFoundException {
        if (res == 0) {
            throw new ResourceNotFoundException("Запись с id = %s не найдена".formatted(id));
        }
        return true;
    }
}
