package org.itmo;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(serviceName = "BoardGameService")
public class BoardGameWebService {
    private final BoardGameDAO dao = new BoardGameDAO();

    @WebMethod(operationName = "searchBoardGames")
    public List<BoardGame> searchBoardGames(
            String name,
            String genre,
            Integer price,
            Integer players
    ) {
        return dao.searchBoardGames(name, genre, price, players);
    }

    @WebMethod(operationName = "deleteBoardGames")
    public boolean deleteBoardGames(Long id) throws ValidationException, ResourceNotFoundException {
        validateId(id);
        return dao.deleteBoardGame(id);
    }

    @WebMethod(operationName = "getBoardGame")
    public BoardGame getBoardGame(Long id) throws ValidationException {
        validateId(id);
        return dao.getBoardGame(id);
    }

    @WebMethod(operationName = "getAllBoardGames")
    public List<BoardGame> getAllBoardGames() {
        return dao.getAllBoardGames();
    }

    @WebMethod(operationName = "createBoardGames")
    public Long createBoardGames(
            String name,
            String genre,
            Integer price,
            Integer minPlayers,
            Integer maxPlayers
    ) throws ValidationException {
        validateGenre(genre);
        validateName(name);
        validatePrice(price);
        validatePlayersAmount(minPlayers, maxPlayers);
        return dao.createBoardGame(name, genre, price, minPlayers, maxPlayers);
    }

    @WebMethod(operationName = "updateBoardGames")
    public boolean updateBoardGames(
            long id,
            String name,
            String genre,
            Integer price,
            Integer minPlayers,
            Integer maxPlayers
    ) throws ValidationException, ResourceNotFoundException {
        validateGenre(genre);
        validateName(name);
        validatePrice(price);
        validatePlayersAmount(minPlayers, maxPlayers);
        return dao.updateBoardGames(name, genre, price, minPlayers, maxPlayers, id);
    }

    private void validateName(String name) throws ValidationException {
        if (name == null || name.isBlank()) {
            throw new ValidationException("Некорректное значение поля name");
        }
    }

    private void validateGenre(String genre) throws ValidationException {
        if (genre == null || genre.isBlank()) {
            throw new ValidationException("Некорректное значение поля name");
        }
    }

    private void validateId(Long id) throws ValidationException {
        if (id == null) {
            throw new ValidationException("ID не может быть пустым");
        }
    }

    private void validatePlayersAmount(Integer minPlayers, Integer maxPlayers) throws ValidationException {
        if (minPlayers == null || maxPlayers == null) {
            return;
        }
        if (minPlayers > maxPlayers) {
            throw new ValidationException("Максимальное количество игроков не может быть больше минимального");
        }
    }

    private void validatePrice(Integer price) throws ValidationException {
        if (price == null) {
            return;
        }
        if (price < 0) {
            throw new ValidationException("Цена не может быть отрицательной");
        }
    }
}