package org.itmo;

import javax.xml.ws.WebFault;

@WebFault(faultBean = "org.itmo.PersonServiceFault")
public class ResourceNotFoundException extends Exception {
    private static final long serialVersionUID = -6647544772732631046L;
    private final BoarGameServiceFault fault;

    public ResourceNotFoundException(String message, BoarGameServiceFault fault) {
        super(message);
        this.fault = fault;
    }

    public ResourceNotFoundException(String message) {
        super(message);
        this.fault = BoarGameServiceFault.defaultInstance(message);
    }

    public ResourceNotFoundException(String message, BoarGameServiceFault fault, Throwable cause) {
        super(message, cause);
        this.fault = fault;
    }

    public BoarGameServiceFault getFaultInfo() {
        return fault;
    }
}
