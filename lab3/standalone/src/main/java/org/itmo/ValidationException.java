package org.itmo;

import javax.xml.ws.WebFault;

@WebFault(faultBean = "org.itmo.PersonServiceFault")
public class ValidationException extends Exception {
    private static final long serialVersionUID = -6647544772732631047L;
    private final BoarGameServiceFault fault;

    public ValidationException(String message, BoarGameServiceFault fault) {
        super(message);
        this.fault = fault;
    }

    public ValidationException(String message) {
        super(message);
        this.fault = BoarGameServiceFault.defaultInstance(message);
    }

    public ValidationException(String message, BoarGameServiceFault fault, Throwable cause) {
        super(message, cause);
        this.fault = fault;
    }

    public BoarGameServiceFault getFaultInfo() {
        return fault;
    }
}
