package org.itmo;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(serviceName = "BoardGameService")
public class BoardGameWebService {
    private final BoardGameDAO dao = new BoardGameDAO();

    @WebMethod(operationName = "searchBoardGames")
    public List<BoardGame> searchBoardGames(
            String name,
            String genre,
            Integer price,
            Integer players
    ) {
        return dao.searchBoardGames(name, genre, price, players);
    }

    @WebMethod(operationName = "deleteBoardGames")
    public boolean deleteBoardGames(Long id) {
        return dao.deleteBoardGame(id);
    }

    @WebMethod(operationName = "getBoardGame")
    public BoardGame getBoardGame(Long id) {
        return dao.getBoardGame(id);
    }

    @WebMethod(operationName = "getAllBoardGames")
    public List<BoardGame> getAllBoardGames() {
        return dao.getAllBoardGames();
    }

    @WebMethod(operationName = "createBoardGames")
    public Long createBoardGames(
            String name,
            String genre,
            Integer price,
            Integer minPlayers,
            Integer maxPlayers
    ) {
        return dao.createBoardGame(name, genre, price, minPlayers, maxPlayers);
    }

    @WebMethod(operationName = "updateBoardGames")
    public boolean updateBoardGames(
            long id,
            String name,
            String genre,
            Integer price,
            Integer minPlayers,
            Integer maxPlayers
    ) {
        return dao.updateBoardGames(name, genre, price, minPlayers, maxPlayers, id);
    }
}