package org.itmo;

import org.itmo.client.BoardGame;
import org.itmo.client.BoardGameService;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

public class Client {
    public static URL url;
    public static Scanner in = new Scanner(System.in);

    static {
        try {
            url = new URL("http://localhost:8081/BoardGameService?wsdl");
            service = new BoardGameService(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public static BoardGameService service;

    public static void main(String[] args) throws MalformedURLException {
        while (true) {
            printMenu();
            int action = Integer.parseInt(in.nextLine());
            switch (action) {
                case 1 -> getAllGames();
                case 2 -> getGame();
                case 3 -> deleteGame();
                case 4 -> updateGame();
                case 5 -> createGame();
                case 0 -> System.exit(0);
                default -> System.out.println("Действия не существует!");
            }

        }

    }

    private static void getAllGames() {
        var res = service.getBoardGameWebServicePort().getAllBoardGames();
        printBoardGames(res);
    }

    private static void getGame() {
        System.out.println("Введите  id записи");
        var id = Long.parseLong(in.nextLine());
        var res = service.getBoardGameWebServicePort().getBoardGame(id);
        printBoardGame(res);
    }

    private static void deleteGame() {
        System.out.println("Введите  id записи");
        var id = Long.parseLong(in.nextLine());
        var res = service.getBoardGameWebServicePort().deleteBoardGames(id);
        System.out.println(res ? "Успех" : "Ошибка");
    }

    private static void updateGame() {
        System.out.println("Введите  id записи");
        var id = Long.parseLong(in.nextLine());
        System.out.println("Введите  имя");
        var name = in.nextLine();
        System.out.println("Введите  жанр");
        var genre = in.nextLine();
        System.out.println("Введите  стоимость");
        var price = Integer.parseInt(in.nextLine());
        System.out.println("Введите  минимальное количество игроков");
        var minPlayers = Integer.parseInt(in.nextLine());
        System.out.println("Введите  максимальное количество игроков");
        var maxPlayers = Integer.parseInt(in.nextLine());
        var res = service.getBoardGameWebServicePort().updateBoardGames(id, name, genre, price, minPlayers, maxPlayers);
        System.out.println(res ? "Успех" : "Ошибка");
    }

    private static void createGame() {
        System.out.println("Введите  имя");
        var name = in.nextLine();
        System.out.println("Введите жанр");
        var genre = in.nextLine();
        System.out.println("Введите  стоимость");
        var price = Integer.parseInt(in.nextLine());
        System.out.println("Введите  минимальное количество игроков");
        var minPlayers = Integer.parseInt(in.nextLine());
        System.out.println("Введите  максимальное количество игроков");
        var maxPlayers = Integer.parseInt(in.nextLine());
        var res = service.getBoardGameWebServicePort().createBoardGames(name, genre, price, minPlayers, maxPlayers);
        System.out.printf("Id новой записи - %s%n", res);
    }


    private static void printMenu() {
        System.out.println("""
                Выбирите действие:
                1. Вывести все игры
                2. Вывести игру по id
                3. Удалить игру
                4. Изменить игру
                5. Создать игру
                0. Выход
                """);
    }

    private static void printBoardGames(List<BoardGame> result) {
        System.out.printf("Total rows %d\n", result.size());
        result.forEach(Client::printBoardGame);

    }

    private static void printBoardGame(BoardGame boardGame) {
        if (boardGame == null) {
            System.out.println("null");
            return;
        }
        System.out.printf("Name: '%s'\t", boardGame.getName());
        System.out.printf("Genre: '%s'\t", boardGame.getGenre());
        System.out.printf("Price: '%d'\t", boardGame.getPrice());
        System.out.printf("Min players: '%d'\t", boardGame.getMinPlayers());
        System.out.printf("Max players: '%d'\n", boardGame.getMaxPlayers());
    }
}